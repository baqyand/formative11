CREATE TABLE `production`.`brand` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `namel` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `production`.`product` 
CHANGE COLUMN `BRAND` `brandId` INT NOT NULL DEFAULT NULL ;

alter table product add foreign key (brandId) references brand(id);

DELETE FROM `production`.`product` WHERE (`id` = '1');
DELETE FROM `production`.`product` WHERE (`id` = '2');
DELETE FROM `production`.`product` WHERE (`id` = '3');
DELETE FROM `production`.`product` WHERE (`id` = '4');
DELETE FROM `production`.`product` WHERE (`id` = '5');
DELETE FROM `production`.`product` WHERE (`id` = '6');
DELETE FROM `production`.`product` WHERE (`id` = '7');
DELETE FROM `production`.`product` WHERE (`id` = '8');
DELETE FROM `production`.`product` WHERE (`id` = '9');
DELETE FROM `production`.`product` WHERE (`id` = '10');
DELETE FROM `production`.`product` WHERE (`id` = '11');
DELETE FROM `production`.`product` WHERE (`id` = '12');
DELETE FROM `production`.`product` WHERE (`id` = '13');
DELETE FROM `production`.`product` WHERE (`id` = '14');
DELETE FROM `production`.`product` WHERE (`id` = '15');
DELETE FROM `production`.`product` WHERE (`id` = '16');
DELETE FROM `production`.`product` WHERE (`id` = '17');
DELETE FROM `production`.`product` WHERE (`id` = '18');
DELETE FROM `production`.`product` WHERE (`id` = '19');
DELETE FROM `production`.`product` WHERE (`id` = '20');
DELETE FROM `production`.`product` WHERE (`id` = '21');
DELETE FROM `production`.`product` WHERE (`id` = '22');

